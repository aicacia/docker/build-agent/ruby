# aicacia/build-agent-ruby:${RUBY_VERSION}

FROM aicacia/docker-kube-helm

ARG RUBY_VERSION=2.6

RUN apt install -y build-essential curl git zlib1g-dev libssl-dev libreadline-dev libyaml-dev libxml2-dev libxslt-dev

ENV RBENV_DIR /usr/local/rbenv

WORKDIR $RBENV_DIR

ENV PATH $RBENV_DIR/.rbenv/shims:$RBENV_DIR/.rbenv/bin:$RBENV_DIR/.rbenv/plugins/ruby-build/bin:$PATH

RUN git clone https://github.com/rbenv/rbenv.git $RBENV_DIR/.rbenv
RUN git clone https://github.com/rbenv/ruby-build.git $RBENV_DIR/.rbenv/plugins/ruby-build

RUN export RUBY_FULL_VERSION="$(rbenv install -l | awk '{print $NF}' | grep "^$RUBY_VERSION" | tail -1)" && \
  rbenv install $RUBY_FULL_VERSION